# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    str.delete!(char) unless char == char.upcase
  end
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  res = ''
  middle = str.length / 2
  if str.length % 2 == 0
    res = str[middle-1..middle]
  else
    res = str[middle]
  end

  res
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_counter = 0

  str.each_char do |char|
    vowel_counter += 1 if VOWELS.include?(char)
  end

  vowel_counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  res = ''
  arr.each_with_index do |el, idx|
    if idx == arr.length - 1
      res << el
    else
      res << el + separator
    end
  end

  res
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  res = ''
  str.each_char.with_index do |char, idx|
    if idx.odd?
      res << char.upcase
    else
      res << char.downcase
    end
  end

  res
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  res = ''
  str.split.each do |word|
    if word.length >= 5
      res << word.reverse + ' '
    else
      res << word + ' '
    end
  end

  res[0...-1]
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a
  arr.each_with_index do |el, idx|
    if el % 3 == 0 && el % 5 == 0
      arr[idx] = 'fizzbuzz'
    elsif el % 3 == 0
      arr[idx] = 'fizz'
    elsif el % 5 == 0
      arr[idx] = 'buzz'
    end
  end

  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse!
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  arr = (1..num).to_a
  res = []
  arr.each do |el|
    if num % el == 0
      res << el
    end
  end
  res == [1, num]
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = (1..num).to_a
  res = []
  arr.each do |el|
    if num % el == 0
      res << el
    end
  end

  res.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select do |el|
    prime?(el)
  end
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = 0
  even = 0
  res_o = 0
  res_e =0

  arr.each do |el|
    if el.odd?
      odd += 1
      res_o = el
    elsif el.even?
      even += 1
      res_e = el
    end
  end

  if odd > 1
    return res_e
  elsif even > 1
    return res_o
  end

end
